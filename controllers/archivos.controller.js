'use strict'
let fs = require('fs');
let Global = require('../global');

function agregarArchivo(req, res) {

    let infoAdicional = req.files.archivo;
    //console.log(infoAdicional);

    if (infoAdicional.path) {

        let path_temp = infoAdicional.path; // Path donde está el archivo con el nombre temporal
        let nombreOriginal = infoAdicional.originalFilename;
        let path_base = path_temp.split('\/')[0];

        //Renombrado de los Archivos
        fs.exists(path_temp, function (exists) {

            if (exists) {

                Global.Global.dirBases.forEach(function (element) {
                    // destination.txt will be created or overwritten by default.
                    console.log(path_base + "/" +element+"/"+nombreOriginal);
                    let nuevoNombre = path_base + "/" +element+"/"+nombreOriginal;
                    fs.copyFile(path_temp, nuevoNombre, (err) => {
                        if (err) throw err;
                        console.log('De: '+path_temp+" a "+ nuevoNombre);
                    });
                });

                console.log("---- Archivo Recibido: "+nombreOriginal+" ------");

                res.status(200).send({ message: 'Archivo Recibido' });
            } else {
                console.log("El archivo NO Existe!!!");
                res.status(400).send({ message: 'Archivo Rechazado' });
            }

        });
    }
}

module.exports = {
    agregarArchivo
}