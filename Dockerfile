FROM node:carbon
RUN apt-get install git-core 
WORKDIR /usr/src/app
RUN git clone https://iepctsergio@bitbucket.org/iepctsergio/api_basearchivos.git
WORKDIR /usr/src/app/api_basearchivos
RUN mkdir assets
RUN mkdir assets/uploads0
RUN mkdir assets/uploads1
RUN mkdir assets/uploads2
RUN mkdir assets/uploads3
RUN mkdir assets/uploads4
RUN mkdir assets/uploads5
RUN npm install
CMD [ "npm", "start" ]