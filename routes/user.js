'use strict'

let express = require('express');

let api = express.Router();

  let multipart = require('connect-multiparty');
  let md_upload = multipart({ uploadDir: './assets/uploads0' });

let archivos    = require('../controllers/archivos.controller');

api.post('/archivos', md_upload, archivos.agregarArchivo);
//api.post('/archivos', md_upload, actas.agregarArchivo);

module.exports = api;